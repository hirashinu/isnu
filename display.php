<?php 
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "fabelio";


// Create Connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check Connection
if ($conn -> connect_error) {
	die("Connection Failed: " . $conn -> connect_error);
}

$sql = "SELECT * FROM article";
$res = $conn -> query($sql);

if ($res -> num_rows > 0) {
	echo "Display Data Successfully"."</br>";
	echo "==================="."</br>";
	while ($row = $res -> fetch_assoc()) {
		#print_r($row);
		echo "ID: ".$row['id']."</br>";
		echo "Title: ".$row['title']."</br>";
		echo "Content: ".$row['content']."</br>";
		echo "Creation Date: ".$row['creation_date']."</br>";
		echo "</br>";
	}
} else {
	echo "Error: " . $sql . "<br>" . $conn -> error;
}

$conn -> close();
?>